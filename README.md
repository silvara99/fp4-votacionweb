# FP4-VotacionWeb-

Proyecto Final de Programación 4

Sistema de votación usando flask, bootstrap (flask-bootstrap) y mongoDB (pymongo)


**Participantes del Grupo:**

Roger Silva / 8-951-948

Jonathan Prado / 8-944-1382

Kenia De Grecia / 8-904-269

Alimeid Meza / 3-737-426

Jhonathan Marin / E-8-132445

Alfredo Guerra / 13-9603-783

Miguel Diaz / PN-004-3355


**Instalar Prerequisitos**

Utilizar la siguiente linea de comandos:

``pip install flask flask-bootstrap pymongo``