#Participantes del Grupo:
#Roger Silva / 8-951-948
#Jonathan Prado / 8-944-1382
#Kenia De Grecia / 8-904-269
#Alimeid Meza / 3-737-426
#Jhonathan Marin / E-8-132445
#Alfredo Guerra / 13-9603-783
#Miguel Diaz / PN-004-3355

#pip install flask flask-bootstrap pymongo
from flask import Flask, redirect, url_for, render_template, request, session, flash, send_file
from flask_bootstrap import Bootstrap
import pymongo

app = Flask (__name__)
app.secret_key = "VvPp1919"
Bootstrap(app)
con = pymongo.MongoClient("mongodb://localhost:27017/")
dblist = con.list_database_names()
cprefill = 20 #Solo de 10 en 10
if "votacion" in dblist:
	print("La base de datos existe, ejectutando servidor web...")
	db = con["votacion"]
	votantes = db["votantes"]
	votoscandidatos = db["votos"]
	votantes.delete_many({})
	votoscandidatos.delete_many({})
	candidatos = [{"_id": 1, "nombre": "Laurentino Cortizo", "partido":"PRD", "votos": int(cprefill/10)}, {"_id": 2, "nombre": "José Isabel Blandon", "partido":'Partido  Popular', "votos": int(cprefill/10)}, {"_id": 3, "nombre": "Laurentino Cortizo", "partido":"Molirena", "votos": int(cprefill/10)}, {"_id": 4, "nombre": "José Isabel Blandon", "partido":"Partido Panameñista", "votos": int(cprefill/10)}, {"_id": 5, "nombre": "Rómulo Roux", "partido":"CD", "votos": int(cprefill/10)}, {"_id": 6, "nombre": "Saúl Mendez", "partido":"FAD", "votos": int(cprefill/10)}, {"_id":7, "nombre": "Rómulo Roux", "partido":"Partido Alianza", "votos": int(cprefill/10)}, {"_id": 8, "nombre": "Ana Matilde Gómez", "partido":"Libre Postulación (celeste)", "votos": int(cprefill/10)}, {"_id": 9, "nombre": "Marco Ameglio", "partido":"Libre Postulación (verde)", "votos": int(cprefill/10)}, {"_id": 10, "nombre": "Ricardo Lombana", "partido":"Libre Postulación (chocolate)", "votos": int(cprefill/10)}]
	votoscandidatos.insert_many(candidatos)
	for i in range(0,cprefill):
		fillvotantes = {"_id": "T-" + str(i+1),"nombre": "Test", "apellido": "#" + str(i+1), "voto":1}
		votantes.insert_one(fillvotantes)
else:
	print("No existe la base de datos")
	exit()

ncedula=None
cedula=None
nombre=None
apellido=None
votante=None
nvoto=None
ncand=None
parcand=None
yr=None
r=0
	
@app.route("/", methods = ["GET", "POST"])
def index():
	if request.method == "GET":
		return render_template ("index.html")
	elif request.method == "POST":
		return render_template ("prohibido.html")
	else:
		return render_template ("error.html")

@app.route("/votar", methods = ["GET", "POST"])
def votar():
	if request.method == "GET":
		return render_template ("votar.html")
	elif request.method == "POST":
		return render_template ("prohibido.html")
	else:
		return render_template ("error.html")

@app.route("/votar/registro", methods = ["GET", "POST"])
def registro():
	if request.method == "GET":
		return render_template ("prohibido.html")
	elif request.method == "POST":
		try:
			global r
			global ncedula
			global cedula
			global nombre
			global apellido
			global votante
			global yr
			r = 0
			yv = 0
			hced = None
			ncedula=None
			cedula=None
			nombre = None
			apellido = None
			ncedula = request.form.get('ced')
			cedula = { "_id": ncedula}
			if votantes.count_documents({}) > 0:
				vc = votantes.find({ "_id": { "$eq": ncedula }})
				for votan in vc:
					try:
						nombre = votan["nombre"]
						apellido = votan ["apellido"]
						yv = votan ["voto"]
						hced = votan["_id"]
					except:
						hced = votan["_id"]
				if yv == 1:
					yr=0
					return render_template ("yavoto.html")
				elif nombre and apellido and yv==0:
					yr=1
					return render_template ("yaregistrado.html")
				elif hced:
					yr=0
				else:
					yr=0
					votantes.insert_one(cedula)
			else:
				yr=0
				votantes.insert_one(cedula)
			return render_template ("registro.html", ced = ncedula)
		except:
			return render_template ("error.html")
	else:
		return render_template ("error.html")
		
@app.route("/votar/lista", methods = ["GET", "POST"])
def listado():
	if request.method == "GET":
		return render_template ("prohibido.html")
	elif request.method == "POST":
		try:
			if yr==0 and r==0:
				global nombre
				global apellido
				global votante
				nombre = request.form.get('nom')
				apellido = request.form.get('ape')
				votante = {"nombre": nombre, "apellido": apellido, "voto": 0}
				votantes.update_one(cedula, {"$set": votante}, upsert=False)			
			return render_template ("listado.html", ced=ncedula, nom=nombre, ape=apellido)
		except:
			return render_template ("error.html")
	else:
		return render_template ("error.html")
		
@app.route("/votar/lista/confirmacion", methods = ["GET", "POST"])
def confirmacion():
	if request.method == "GET":
		return render_template ("prohibido.html")
	elif request.method == "POST":
		try:
			global r
			global nvoto
			global ncand
			global nomcand
			global parcand
			r=1
			nvoto = int(request.form.get('voto'))
			ncand = {"_id": nvoto}
			query = { "_id": { "$eq": nvoto } }
			rcandidato =  votoscandidatos.find(query)
			for candidato in rcandidato:
				nomcand = candidato["nombre"]
				parcand = candidato["partido"]
			return render_template ("confirmacion.html", ced=ncedula, nom=nombre, ape=apellido, voto=nvoto, ncand = nomcand, np=parcand)
		except:
			return render_template ("error.html")
	else:
		return render_template ("error.html")

@app.route("/votar/realizado", methods = ["GET", "POST"])
def realizado():
	if request.method == "GET":
		return render_template ("prohibido.html")
	elif request.method == "POST":
		try:
			votantes.update_one(cedula, {"$set": {"voto": 1}}, upsert=False)
			query = { "_id": { "$eq": nvoto } }
			rcandidato =  votoscandidatos.find(query)
			for candidato in rcandidato:
				nvotocand = (candidato["votos"])
				nvotocand = nvotocand + 1
				votoscandidatos.update_one(ncand, {"$set": {"votos": nvotocand}}, upsert=False)
			return render_template ("realizado.html", voto=nvoto, ncand = nomcand, np=parcand)
		except:
			return render_template ("error.html")
	else:
		return render_template ("error.html")
		
@app.route("/resultado", methods = ["GET", "POST"])
def resultado():
	if request.method == "GET":
		try:
			totalvotos = 0
			porcentajevotos = 0
			cr=[""]
			ncr=[""]
			npr=[""]
			vr=[""]
			pv=[""]
			queryvotantes= {"voto":{"$eq": 1}}
			for candidatos in votoscandidatos.find():
				cr.append(candidatos["_id"])
				ncr.append(candidatos["nombre"])
				npr.append(candidatos["partido"])
				vr.append(candidatos["votos"])
				try:
					totalvotos = totalvotos + candidatos["votos"]
				except:
					totalvotos = totalvotos
				
			for candidatos in votoscandidatos.find():
				try:
					porcentajevotos = round((candidatos["votos"] / totalvotos) * 100, 2)
				except:
					porcentajevotos = 10
				pv.append(porcentajevotos)
			return render_template ("resultado.html", c1=cr[1], nc1=ncr[1], pc1=npr[1], v1=vr[1], pv1=pv[1], c2=cr[2], nc2=ncr[2], pc2=npr[2], v2=vr[2], pv2=pv[2], c3=cr[3], nc3=ncr[3], pc3=npr[3], v3=vr[3], pv3=pv[3], c4=cr[4], nc4=ncr[4], pc4=npr[4], v4=vr[4], pv4=pv[4], c5=cr[5], nc5=ncr[5], pc5=npr[5], v5=vr[5], pv5=pv[5], c6=cr[6], nc6=ncr[6], pc6=npr[6], v6=vr[6], pv6=pv[6], c7=cr[7], nc7=ncr[7], pc7=npr[7], v7=vr[7], pv7=pv[7], c8=cr[8], nc8=ncr[8], pc8=npr[8], v8=vr[8], pv8=pv[8], c9=cr[9], nc9=ncr[9], pc9=npr[9], v9=vr[9], pv9=pv[9], c10=cr[10], nc10=ncr[10], pc10=npr[10], v10=vr[10], pv10=pv[10], votantes=votantes.count_documents(queryvotantes))
		except:
			return render_template ("error.html")
	elif request.method == "POST":
		return render_template ("prohibido.html")
	else:
		return render_template ("error.html")
		
@app.route("/resultado/posicion", methods = ["GET", "POST"])
def posicion():
	if request.method == "GET":
		try:
			totalvotos = 0
			porcentajevotos = 0
			cr=[""]
			ncr=[""]
			npr=[""]
			vr=[""]
			pv=[""]
			queryvotantes= {"voto":{"$eq": 1}}
			for candidatos in votoscandidatos.find().sort("votos", -1):
				cr.append(candidatos["_id"])
				ncr.append(candidatos["nombre"])
				npr.append(candidatos["partido"])
				vr.append(candidatos["votos"])
				try:
					totalvotos = totalvotos + candidatos["votos"]
				except:
					totalvotos = totalvotos
				
			for candidatos in votoscandidatos.find().sort("votos", -1):
				try:
					porcentajevotos = round((candidatos["votos"] / totalvotos) * 100, 2)
				except:
					porcentajevotos = 10
				pv.append(porcentajevotos)
			return render_template ("posicion.html", c1=cr[1], nc1=ncr[1], pc1=npr[1], v1=vr[1], pv1=pv[1], c2=cr[2], nc2=ncr[2], pc2=npr[2], v2=vr[2], pv2=pv[2], c3=cr[3], nc3=ncr[3], pc3=npr[3], v3=vr[3], pv3=pv[3], c4=cr[4], nc4=ncr[4], pc4=npr[4], v4=vr[4], pv4=pv[4], c5=cr[5], nc5=ncr[5], pc5=npr[5], v5=vr[5], pv5=pv[5], c6=cr[6], nc6=ncr[6], pc6=npr[6], v6=vr[6], pv6=pv[6], c7=cr[7], nc7=ncr[7], pc7=npr[7], v7=vr[7], pv7=pv[7], c8=cr[8], nc8=ncr[8], pc8=npr[8], v8=vr[8], pv8=pv[8], c9=cr[9], nc9=ncr[9], pc9=npr[9], v9=vr[9], pv9=pv[9], c10=cr[10], nc10=ncr[10], pc10=npr[10], v10=vr[10], pv10=pv[10], votantes=votantes.count_documents(queryvotantes))
		except:
			return render_template ("error.html")
	elif request.method == "POST":
		return render_template ("prohibido.html")
	else:
		return render_template ("error.html")

@app.route("/musica.mp3", methods = ["GET", "POST"])
def dmusic():
	if request.method == "GET":
		return send_file('static/musica.mp3', attachment_filename='musica.mp3')
		
@app.route("/video.mp4", methods = ["GET", "POST"])
def dvideo():
	if request.method == "GET":
		return send_file('static/video.mp4', attachment_filename='video.mp4')

		
if __name__ == "__main__":
	app.run(host="0.0.0.0",port=80) //inseguro